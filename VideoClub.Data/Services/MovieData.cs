﻿using System.Data.Entity;
using VideoClub.Data.Models;

namespace VideoClub.Data.Services
{
    public class MovieData : DbContext
    {
        public MovieData(string url):base(url)
        {
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieCopy> MovieCopies { get; set; }
    }
}
