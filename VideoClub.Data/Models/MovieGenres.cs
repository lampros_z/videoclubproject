﻿namespace VideoClub.Data.Models
{
    public class MovieGenres
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public int Genre { get; set; }
    }
}
