﻿using System.Collections.Generic;

namespace VideoClub.Data.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public virtual ICollection<MovieGenres> genres { get; set; }
        public virtual ICollection<MovieCopy> movieCopies { get; set; }

        public Movie()
        {
            genres = new List<MovieGenres>();
            movieCopies = new List<MovieCopy>();
        }
    }
}