﻿using System.Web.Mvc;

namespace VideoClub.Data.Models
{
    public class MovieCopy
    {
        public int Id{ get; set; }
        public bool IsAvailable{ get; set; }
        public int? MovieId { get; set; }
        public int? MovieRentalRecordId { get; set; }
    }
}