﻿namespace VideoClub.Data.Models
{
    public enum Genre
    {
        None,
        Action,
        Adventure,
        Crime,
        Mystery,
        Comedy,
        Drama,
        Fantasy,
        Historical,
        Horror,
        Philosophical,
        Political,
        Romance
    }

}