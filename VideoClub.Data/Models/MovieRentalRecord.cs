﻿using System;
using System.Collections.Generic;

namespace VideoClub.Data.Models
{
    public class MovieRentalRecord
    {
        public int Id{ get; set; }
        public DateTime RentDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public bool Returned { get; set; }
        public string Comment { get; set; }
        public ICollection<MovieCopy> movieCopies { get; set; }

        public string ClientUserId { get; set; }

        public MovieRentalRecord()
        {
            ClientUserId = Guid.NewGuid().ToString();
        }
    }
}