﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using VideoClub.Data.Models;
using VideoClub.Web.Infrastructure;
using VideoClub.Web.Models;

namespace VideoClub.Web.UtilityClasses
{
    public class UserControllerUtils
    {
        public static void HandleUserFavouriteMovieGenre(ClientUser user, UserModel userModel)
        {
            if (userModel.ActionChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Action });
            if (userModel.AdventureChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Adventure });
            if (userModel.CrimeChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Crime });
            if (userModel.MysteryChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Mystery });
            if (userModel.ComedyChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Comedy });
            if (userModel.DramaChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Drama });
            if (userModel.FantasyChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Fantasy });
            if (userModel.HistoricalChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Historical });
            if (userModel.HorrorChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Horror });
            if (userModel.PhilosophicalChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Philosophical });
            if (userModel.PoliticalChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Political });
            if (userModel.RomanceChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Romance });
            if (user.FavouriteMovieGenres.Count == 0)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.None });

            /*System.Diagnostics.Debug.WriteLine(userModel.ActionChecked + " action");
            System.Diagnostics.Debug.WriteLine(userModel.AdventureChecked + " adve");
            System.Diagnostics.Debug.WriteLine(userModel.CrimeChecked + " crime");
            System.Diagnostics.Debug.WriteLine(userModel.MysteryChecked + " mustery");
            System.Diagnostics.Debug.WriteLine(userModel.ComedyChecked + " comedy");
            System.Diagnostics.Debug.WriteLine(userModel.DramaChecked + " drama");
            System.Diagnostics.Debug.WriteLine(userModel.FantasyChecked + " fantasy");
            System.Diagnostics.Debug.WriteLine(userModel.HistoricalChecked + " hist");
            System.Diagnostics.Debug.WriteLine(userModel.PhilosophicalChecked + " phil");
            System.Diagnostics.Debug.WriteLine(userModel.PoliticalChecked + " pol");
            System.Diagnostics.Debug.WriteLine(userModel.RomanceChecked + " rom");
            System.Diagnostics.Debug.WriteLine(userModel.HorrorChecked + " hor");*/
        }

        public static async Task<IdentityRole> GetUserRoleAsync(ApplicationRoleManager roleManager)
        {
            var role  = await roleManager.FindByNameAsync("Client");
            if(role == null)
            {
                var identityResult = await roleManager.CreateAsync(new IdentityRole { Name = "Client" });
                if(identityResult.Succeeded)
                {
                    role = await roleManager.FindByNameAsync("Client");
                    return role;
                }
                return null;
            }
            return role;
        }




    }
}