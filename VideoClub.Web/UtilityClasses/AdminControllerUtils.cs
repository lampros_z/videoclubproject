﻿namespace VideoClub.Web.UtilityClasses
{
    public class AdminControllerUtils
    {
        public static int CalculatePagination(int TotalRecords, int RecordsPerPage)
        {
            int result;
            result = TotalRecords / RecordsPerPage;
            if (TotalRecords % RecordsPerPage != 0)
                result += 1;
            return result;
        }
    }
}