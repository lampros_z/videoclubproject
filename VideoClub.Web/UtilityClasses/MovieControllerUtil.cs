﻿using System;
using System.Linq;
using VideoClub.Data.Models;
using VideoClub.Web.Models;

namespace VideoClub.Web.UtilityClasses
{
    public class MovieControllerUtil
    {
        public static void HandleMovieGenre(Movie movie, MovieModel model)
        {
            if (model.ActionChecked)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.Action });
            if (model.AdventureChecked)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.Adventure });
            if (model.CrimeChecked)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.Crime });
            if (model.MysteryChecked)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.Mystery });
            if (model.ComedyChecked)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.Comedy });
            if (model.DramaChecked)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.Drama });
            if (model.FantasyChecked)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.Fantasy });
            if (model.HistoricalChecked)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.Historical });
            if (model.HorrorChecked)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.Horror });
            if (model.PhilosophicalChecked)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.Philosophical });
            if (model.PoliticalChecked)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.Political });
            if (model.RomanceChecked)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.Romance });
            if (movie.genres.Count == 0)
                movie.genres.Add(new MovieGenres { Genre = (int)Genre.None });
        }
    }
}