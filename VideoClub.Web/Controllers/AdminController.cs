﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoClub.Data.Models;
using VideoClub.Data.Services;
using VideoClub.Web.Models;
using VideoClub.Web.UtilityClasses;
using VideoClub.Web.Extensions;
using System.Data.Entity.Migrations;

namespace VideoClub.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private UserManager<ClientUser> _userManager => HttpContext.GetOwinContext().Get<UserManager<ClientUser>>();
        private string url = ConfigurationManager.AppSettings["dbConnection"];
        private int numberOfMoviesPerPage = 10;
        private int numberOfRentalRecordsPerPage = 10;
        private int numberOfClientsPerPage = 10;
        private int numberOfRentalHistoryRecordsPerPage = 10;
        private int numberOfActiveRentalHistoryRecordsPerPage = 10;

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListClients(int? pageNo)
        {
            var ClientCount = 0;
            int page = 1;
            if (pageNo != null)
                page = (int)pageNo;
            //Get available client users (all for now)
            var adminUsername = ConfigurationManager.AppSettings["AdminUsername"];
            var clientUsers = _userManager.Users.Where(u => u.UserName != "Admin").OrderBy(u=>u.UserName).Skip(numberOfClientsPerPage*(page-1)).Take(numberOfClientsPerPage).ToArray();

            //Create ClientUserModel
            var models = new List<ClientUserModel>();
            foreach(var client in clientUsers)
            {
                models.Add(new ClientUserModel { Id = client.Id, Username = client.UserName, FirstName = client.FirstName, LastName = client.LastName, RentalRecordsCount = client.movieRentalRecords.Where(m=>m.Returned == false).Count() });
            }
            ClientCount = AdminControllerUtils.CalculatePagination(_userManager.Users.Where(u => u.UserName != "Admin").Count(), numberOfClientsPerPage);
            ViewData["pagination"] = ClientCount;
            ViewData["page"] = page;
            return View(models);
        }

        public ActionResult ListRentalRecordsHistory(string Id, int? pageNo)
        {
            var RecordCount = 0;
            int page = 1;
            if (pageNo != null)
                page = (int)pageNo;
            var user = _userManager.FindById(Id);
            if (user == null)
                return RedirectToAction("Index", "Admin");
            var rentalRecords = user.movieRentalRecords.OrderByDescending(r=>r.RentDate).Skip(numberOfRentalHistoryRecordsPerPage*(page-1)).Take(numberOfRentalHistoryRecordsPerPage).ToArray();
            var model = new List<ClientRentalRecordModel>();
            foreach(var record in rentalRecords)
            {
                model.Add(new ClientRentalRecordModel { 
                    UserId = user.Id,
                    Username = user.UserName,
                    RentalRecordId = record.Id,
                    RentDate = record.RentDate,
                    ReturnDate = record.ReturnDate,
                    Returned = record.Returned,
                    Comment = record.Comment
                    }
                );
            }
            RecordCount = AdminControllerUtils.CalculatePagination(user.movieRentalRecords.Count(), numberOfRentalHistoryRecordsPerPage);
            //refactor MVVM
            //Automapper
            ViewData["pagination"] = RecordCount;
            ViewData["page"] = page;
            return View(model);
        }

        public ActionResult RentMovie(string id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult RentMovie(string UserId, string CommentText,MovieRentalRecordPostDataModel[] movies)
        {
            if(UserId == null || movies == null || movies.Length == 0)
                return Json("[{}]", JsonRequestBehavior.AllowGet);
            System.Diagnostics.Debug.WriteLine(CommentText);
            var records = new List<MovieCopy>();
            IQueryable<MovieCopy> query;
            using (var context = new MovieData(url))
            {
                foreach (var m in movies)
                {
                    query = context.MovieCopies.Where(mc => mc.MovieId == m.Id).Where(mc => mc.IsAvailable == true).Take(m.Quantity);
                    var result = query.ToList();
                    result.ForEach(item =>
                    {
                        item.IsAvailable = false;
                        records.Add(item);
                    });
                }

                ClientUser user = _userManager.Users.Where(u => u.Id == UserId).First();
                MovieRentalRecord movieRentalRecord = new MovieRentalRecord
                {
                    RentDate = DateTime.Today,
                    ReturnDate = DateTime.Today.AddDays(7),
                    Comment = CommentText,
                    Returned = false,
                    ClientUserId = UserId,
                };
                user.movieRentalRecords.Add(movieRentalRecord);
                _userManager.Update(user);
                records.ForEach(record =>
                {
                    record.IsAvailable = false;
                    record.MovieRentalRecordId = movieRentalRecord.Id;
                    context.MovieCopies.AddOrUpdate(record);
                    context.SaveChanges();
                });
                
            }
            return Json(records, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchMovie(string Title)
        {
            if (Title == null || Title.Trim(' ') == "")
                return Json("", JsonRequestBehavior.AllowGet);
            var movies = new List<MovieListModel>();
            using (var context = new MovieData(url))
            {
                var MovieRecords = context.Movies.Where(m => m.Title.StartsWith(Title)).Where(m => m.movieCopies.Count() > 0).Take(50).ToList();
                foreach(var record in MovieRecords)
                {
                    movies.Add(
                        new MovieListModel { Title = record.Title, Available = record.movieCopies.Where(mc=>mc.IsAvailable == true).Count(), MovieId = record.Id}
                   );
                }
            }
            return Json(movies, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ListActiveRentalRecords(int? pageNo)
        {
            var RentalRecordCount = 0;
            int page = 1;
            if (pageNo != null)
                page = (int)pageNo;
            var users = _userManager.Users;
            if (users == null)
            {
                return View(new List<ClientRentalRecordModel>());
            }

            var model = new List<ClientRentalRecordModel>();
            var usersdata = users.ToArray();
            foreach (var user in usersdata)
            {
                var rentalRecords = user.movieRentalRecords.Where(r => r.Returned == false).Skip(numberOfRentalRecordsPerPage*(page-1)).Take(numberOfRentalRecordsPerPage).ToArray();
                foreach (var record in rentalRecords)
                {
                    model.Add(new ClientRentalRecordModel
                    {
                        UserId = user.Id,
                        Username = user.UserName,
                        RentalRecordId = record.Id,
                        RentDate = record.RentDate,
                        ReturnDate = record.ReturnDate,
                        Returned = record.Returned,
                        Comment = record.Comment
                    }
                    );
                }
            }
            RentalRecordCount = AdminControllerUtils.CalculatePagination(_userManager.Users.SelectMany(u=>u.movieRentalRecords).Where(r=>r.Returned == false).Count(), numberOfRentalRecordsPerPage);
            ViewData["pagination"] = RentalRecordCount;
            ViewData["page"] = page;
            return View(model);
        }

        public ActionResult ListActiveRentalRecordsHistory(string Id, int? pageNo)
        {
            int RentalRecordCount = 0;
            int page = 1;
            if (pageNo != null)
                page = (int)pageNo;
            var users = _userManager.Users;
            var user = _userManager.FindById(Id);
            if (user == null)
                return RedirectToAction("Index", "Admin");
            var rentalRecords = user.movieRentalRecords.Where( r => r.Returned == false ).OrderByDescending(r => r.RentDate).Skip(numberOfActiveRentalHistoryRecordsPerPage*(page - 1)).Take(numberOfActiveRentalHistoryRecordsPerPage).ToArray();
            var model = new List<ClientRentalRecordModel>();
            foreach (var record in rentalRecords)
            {
                model.Add(new ClientRentalRecordModel
                {
                    UserId = user.Id,
                    Username = user.UserName,
                    RentalRecordId = record.Id,
                    RentDate = record.RentDate,
                    ReturnDate = record.ReturnDate,
                    Returned = record.Returned,
                    Comment = record.Comment
                }
                );
            }
            RentalRecordCount = AdminControllerUtils.CalculatePagination(user.movieRentalRecords.Where(r => r.Returned == false).Count(), numberOfActiveRentalHistoryRecordsPerPage);
            ViewData["pagination"] = RentalRecordCount;
            ViewData["page"] = page;
            return View(model);
        }

        public ActionResult ViewSettings()
        {
            return View();
        }

        public ActionResult AddMovie()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult AddMovie(MovieModel model)
        {
            var movie = new Movie();
            
           
            MovieControllerUtil.HandleMovieGenre(movie, model);
            MovieCopy mvcpy;
            for (int i = 0; i < model.MovieCopies; i++)
            {
                mvcpy = new MovieCopy
                {
                    IsAvailable = true,
                };
                movie.movieCopies.Add(mvcpy);
            }
            using (var context = new MovieData(url))
            {
                movie.Title = model.Title;
                movie.Description = model.Description;
                movie.ImageUrl = model.ImageUrl;
                movie.YoutubeUrl = model.YoutubeUrl;
                context.Movies.Add(movie);
                int rows = context.SaveChanges();
                if (rows <= 0)
                    throw new Exception("Database insertion error: Movie");
            }
            return View();
        }

        public ActionResult Movies(int? pageNo, string title, int[] genres, bool[] availability)
        {
           
            var model = new List<MovieDataModel>();
            var MovieCount = 0;
            int page = 1;
            if (pageNo != null)
                page = (int)pageNo;
            using (var dbContext = new MovieData(url))
            {

                IQueryable<Movie> query;
                if (title == null || title.Trim(' ') == "")
                {
                    query =
                       dbContext.Movies
                      .OrderBy(m => m.Id)
                      .HandleAvailabilityFilter(availability)
                      .HandleGenreFilter(genres)
                      .Skip(numberOfMoviesPerPage * (page - 1))
                      .Take(numberOfMoviesPerPage);
                }
                else
                {
                    query =
                    dbContext.Movies
                    .Where(m => m.Title.ToLower().StartsWith(title.ToLower()))
                    .HandleAvailabilityFilter(availability)
                    .HandleGenreFilter(genres)
                   .OrderBy(m => m.Id)
                   .Skip(numberOfMoviesPerPage * (page - 1))
                   .Take(numberOfMoviesPerPage);
                }
                
                var movies = query.ToArray();
                foreach(var m in movies)
                {
                    var mvd = new MovieDataModel
                    {
                        Id = m.Id,
                        Title = m.Title,
                        Description = m.Description,
                        ImageUrl = m.ImageUrl,
                        YoutubeUrl = m.YoutubeUrl,
                        Genres = m.genres.ToList(),
                        AvailableMovieCopies = m.movieCopies.Where(mc => mc.IsAvailable == true).Count()
                    };
                    mvd.TransformGenresToString();
                    model.Add(mvd);
                }
                if(title == null || title.Trim(' ') == "")
                    MovieCount = AdminControllerUtils.CalculatePagination(dbContext.Movies.HandleAvailabilityFilter(availability).HandleGenreFilter(genres).Count(), numberOfMoviesPerPage);
                else
                    MovieCount = AdminControllerUtils.CalculatePagination(dbContext.Movies.Where(m => m.Title.ToLower().StartsWith(title.ToLower())).HandleAvailabilityFilter(availability).HandleGenreFilter(genres).Count(), numberOfMoviesPerPage);
            }
            ViewData["pagination"] = MovieCount;
            if (title != null)
                ViewData["search"] = Uri.EscapeDataString(title);
            else
                ViewData["search"] = "";
            ViewData["page"] = page;
            return View(model);
        }
    }
}