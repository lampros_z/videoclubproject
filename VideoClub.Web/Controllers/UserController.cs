﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VideoClub.Data.Models;
using VideoClub.Data.Services;
using VideoClub.Web.Extensions;
using VideoClub.Web.Infrastructure;
using VideoClub.Web.Models;
using VideoClub.Web.UtilityClasses;

namespace VideoClub.Web.Controllers
{
    public class UserController : Controller
    {
        private UserManager<ClientUser> UserManager => HttpContext.GetOwinContext().Get<UserManager<ClientUser>>();
        private SignInManager<ClientUser, string> signInManager => HttpContext.GetOwinContext().Get<SignInManager<ClientUser, string>>();
        private ApplicationRoleManager _AppRoleManager => Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
        private string url = ConfigurationManager.AppSettings["dbConnection"];
        private int numberOfMoviesPerPage = 10;
        private int numberOfMoviesCardsPerPage = 4;

        [Authorize(Roles = "Client")]
        public ActionResult Index()
        {
            var username = signInManager.AuthenticationManager.User.Identity.Name;
            var clientUser = UserManager.FindByName(username);

            return View(clientUser);
        }

        [HttpGet]
        public ActionResult Register()
        {
            return Index();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(UserModel userModel)
        {
            var identityUser = await UserManager.FindByNameAsync(userModel.Email);
            if (identityUser != null)
            {
                return RedirectToAction("Index", "Home");
            }


            var user = new ClientUser(userModel.FirstName, userModel.LastName, userModel.Email);

            UserControllerUtils.HandleUserFavouriteMovieGenre(user, userModel);

            var identityResult = await UserManager.CreateAsync(user, userModel.Password);

            if (!identityResult.Succeeded)
            {
                foreach (var err in identityResult.Errors)
                    System.Diagnostics.Debug.WriteLine(err);
                ModelState.AddModelError("", identityResult.Errors.FirstOrDefault());
                return View(userModel);
            }
            
            var identityRole = await UserControllerUtils.GetUserRoleAsync(_AppRoleManager);
            identityUser = await UserManager.FindByNameAsync(userModel.Email);
            var idRole = new IdentityUserRole { UserId = identityUser.Id, RoleId = identityRole.Id};
            user.Roles.Add(idRole);
            IdentityResult identityResult1 = await UserManager.UpdateAsync(user);
            if (!identityResult1.Succeeded)
            {
                foreach (var err in identityResult.Errors)
                    System.Diagnostics.Debug.WriteLine(err);
                ModelState.AddModelError("", identityResult.Errors.FirstOrDefault());
                return View(userModel);
            }

            return RedirectToAction("Index", "User");
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel login)
        {
            var signInStatus = await signInManager.PasswordSignInAsync(login.Email, login.Password, true, false);
            if(signInStatus == SignInStatus.Failure)
            {
                ModelState.AddModelError("", "Invalid Credentials");
                return RedirectToAction("Index", "Home");
            }
            
            var identityUser = await UserManager.FindByNameAsync(login.Email);
            ICollection<IdentityUserRole> IdUserRole = identityUser.Roles;
            ICollection<IdentityRole> UserRoles = _AppRoleManager.Roles.ToList();
            string role = UserRoles.Join(IdUserRole, ur => ur.Id, iur => iur.RoleId, (r, iur)=>r.Name).First();
           
            if (role == "Admin")
                return RedirectToAction("Index", "Admin");
            else if (role == "Client")
                return RedirectToAction("Index", "User");
            else
                return RedirectToAction("Index", "Home");
        }
        
        [HttpGet]
        public ActionResult Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [Authorize(Roles = "Client")]
        [HttpGet]
        public ActionResult Movies(int? pageNo, string title, int[] genres, bool[] availability)
        {
            var model = new List<MovieDataModel>();
            var MovieCount = 0;
            int page = 1;
            if (pageNo != null)
                page = (int)pageNo;
            using (var dbContext = new MovieData(url))
            {

                IQueryable<Movie> query;
                if (title == null || title.Trim(' ') == "")
                {
                    query =
                       dbContext.Movies
                      .OrderBy(m => m.Id)
                      .HandleAvailabilityFilter(availability)
                      .HandleGenreFilter(genres)
                      .Skip(numberOfMoviesPerPage * (page - 1))
                      .Take(numberOfMoviesPerPage);
                }
                else
                {
                    query =
                    dbContext.Movies
                    .Where(m => m.Title.ToLower().StartsWith(title.ToLower()))
                    .HandleAvailabilityFilter(availability)
                    .HandleGenreFilter(genres)
                   .OrderBy(m => m.Id)
                   .Skip(numberOfMoviesPerPage * (page - 1))
                   .Take(numberOfMoviesPerPage);
                }

                var movies = query.ToArray();
                foreach (var m in movies)
                {
                    var mvd = new MovieDataModel
                    {
                        Id = m.Id,
                        Title = m.Title,
                        Description = m.Description,
                        ImageUrl = m.ImageUrl,
                        YoutubeUrl = m.YoutubeUrl,
                        Genres = m.genres.ToList(),
                        AvailableMovieCopies = m.movieCopies.Where(mc => mc.IsAvailable == true).Count()
                    };
                    mvd.TransformGenresToString();
                    model.Add(mvd);
                }
                if (title == null || title.Trim(' ') == "")
                    MovieCount = AdminControllerUtils.CalculatePagination(dbContext.Movies.HandleAvailabilityFilter(availability).HandleGenreFilter(genres).Count(), numberOfMoviesPerPage);
                else
                    MovieCount = AdminControllerUtils.CalculatePagination(dbContext.Movies.Where(m => m.Title.ToLower().StartsWith(title.ToLower())).HandleAvailabilityFilter(availability).HandleGenreFilter(genres).Count(), numberOfMoviesPerPage);
            }
            ViewData["pagination"] = MovieCount;
            if (title != null)
                ViewData["search"] = Uri.EscapeDataString(title);
            else
                ViewData["search"] = "";
            ViewData["page"] = page;
            return View(model);
        }

        [Authorize(Roles = "Client")]
        [HttpGet]
        public ActionResult SearchMovie(string Title)
        {
            if (Title == null || Title.Trim(' ') == "")
                return Json("", JsonRequestBehavior.AllowGet);
            var movies = new List<MovieListModel>();
            using (var context = new MovieData(url))
            {
                var MovieRecords = context.Movies.Where(m => m.Title.StartsWith(Title)).Where(m => m.movieCopies.Count() > 0).Take(50).ToList();
                foreach (var record in MovieRecords)
                {
                    movies.Add(
                        new MovieListModel { Title = record.Title, Available = record.movieCopies.Where(mc => mc.IsAvailable == true).Count(), MovieId = record.Id }
                   );
                }
            }
            return Json(movies, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MovieCards(int? pageNo, string title, int[] genres, bool[] availability)
        {
            var model = new List<MovieDataModel>();
            var MovieCount = 0;
            int page = 1;
            if (pageNo != null)
                page = (int)pageNo;
            using (var dbContext = new MovieData(url))
            {

                IQueryable<Movie> query;
                if (title == null || title.Trim(' ') == "")
                {
                    query =
                       dbContext.Movies
                      .OrderBy(m => m.Id)
                      .HandleAvailabilityFilter(availability)
                      .HandleGenreFilter(genres)
                      .Skip(numberOfMoviesCardsPerPage * (page - 1))
                      .Take(numberOfMoviesCardsPerPage);
                }
                else
                {
                    query =
                    dbContext.Movies
                    .Where(m => m.Title.ToLower().StartsWith(title.ToLower()))
                    .HandleAvailabilityFilter(availability)
                    .HandleGenreFilter(genres)
                   .OrderBy(m => m.Id)
                   .Skip(numberOfMoviesCardsPerPage * (page - 1))
                   .Take(numberOfMoviesCardsPerPage);
                }

                var movies = query.ToArray();
                foreach (var m in movies)
                {
                    var mvd = new MovieDataModel
                    {
                        Id = m.Id,
                        Title = m.Title,
                        Description = m.Description,
                        ImageUrl = m.ImageUrl,
                        YoutubeUrl = m.YoutubeUrl,
                        Genres = m.genres.ToList(),
                        AvailableMovieCopies = m.movieCopies.Where(mc => mc.IsAvailable == true).Count()
                    };
                    mvd.TransformGenresToString();
                    model.Add(mvd);
                }
                if (title == null || title.Trim(' ') == "")
                    MovieCount = AdminControllerUtils.CalculatePagination(dbContext.Movies.HandleAvailabilityFilter(availability).HandleGenreFilter(genres).Count(), numberOfMoviesCardsPerPage);
                else
                    MovieCount = AdminControllerUtils.CalculatePagination(dbContext.Movies.Where(m => m.Title.ToLower().StartsWith(title.ToLower())).HandleAvailabilityFilter(availability).HandleGenreFilter(genres).Count(), numberOfMoviesCardsPerPage);
            }
            ViewData["pagination"] = MovieCount;
            if (title != null)
                ViewData["search"] = Uri.EscapeDataString(title);
            else
                ViewData["search"] = "";
            ViewData["page"] = page;
            return View(model);
        }

    }
}