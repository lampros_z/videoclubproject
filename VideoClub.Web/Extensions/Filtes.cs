﻿using Microsoft.Ajax.Utilities;
using System.Linq;
using VideoClub.Data.Models;

namespace VideoClub.Web.Extensions
{
    public static class Filtes
    {
        public static IQueryable<Movie> HandleAvailabilityFilter(this IQueryable<Movie> query, bool[] availabilty)
        {
            if (query == null)
                return null;
            if (availabilty == null)
                return query;
            foreach (var item in availabilty)
            {
                if (item)
                {
                    query = query.Where(m => m.movieCopies.Any(mc => mc.IsAvailable == true));
                }
                else
                {
                    query = query.Where(m => m.movieCopies.All(mc => mc.IsAvailable == false));
                }
            }
            return query;
        }

        public static IQueryable<Movie> HandleGenreFilter(this IQueryable<Movie> query, int[] genres)
        {
            if (genres == null)
                return query;
            foreach (var genre in genres)
            {
                query = query.Where(m => m.genres.Any(g => g.Genre == genre));
            }
            return query;
        }

    }
}