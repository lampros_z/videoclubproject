﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Profile;
using VideoClub.Data.Models;

namespace VideoClub.Web.Models
{
    public class MovieDataModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public ICollection<MovieGenres> Genres { get; set; }
        [Display(Name = "Genre")]
        public string GenreString { get;set;}
        public ICollection<MovieCopy> movieCopies { get; set; }
        public int AvailableMovieCopies { get; set; }

        public string TransformGenresToString()
        {
            var str = "";
            foreach(var g in Genres)
            {
                switch (g.Genre)
                {
                    case (int)Genre.Action:
                        str += "Action, ";
                        break;
                    case (int)Genre.Adventure:
                        str += "Adventure, ";
                        break;
                    case (int)Genre.Crime:
                        str += "Crime, ";
                        break;
                    case (int)Genre.Mystery:
                        str += "Mystery, ";
                        break;
                    case (int)Genre.Comedy:
                        str += "Comedy, ";
                        break;
                    case (int)Genre.Drama:
                        str += "Drama, ";
                        break;
                    case (int)Genre.Fantasy:
                        str += "Fantasy, ";
                        break;
                    case (int)Genre.Historical:
                        str += "Historical, ";
                        break;
                    case (int)Genre.Horror:
                        str += "Horror, ";
                        break;
                    case (int)Genre.Philosophical:
                        str += "Philosophical, ";
                        break;
                    case (int)Genre.Political:
                        str += "Political, ";
                        break;
                    case (int)Genre.Romance:
                        str += "Romance, ";
                        break;
                    case (int)Genre.None:
                        str += "";
                        break;
                }
            }
            if (str.Length > 2)
                GenreString = str.Substring(0, str.Length - 2);
            else
                GenreString = str;
            return str;
        }
    }
}