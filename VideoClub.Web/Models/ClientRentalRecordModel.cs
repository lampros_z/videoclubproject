﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoClub.Web.Models
{
    public class ClientRentalRecordModel
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public int RentalRecordId { get; set; }
        public DateTime RentDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public bool Returned { get; set; }
        public string Comment { get; set; }
    }
}