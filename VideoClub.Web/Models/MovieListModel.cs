﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoClub.Web.Models
{
    public class MovieListModel
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public int Available { get; set; }
    }
}