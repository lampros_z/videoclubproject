﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoClub.Web.Models
{
    public class MovieRentalRecordPostDataModel
    {
        public int? Id { get; set; }
        public int Quantity { get; set; }
    }
}